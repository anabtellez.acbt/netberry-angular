import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EtiquetasComponent } from './etiquetas/etiquetas.component';
import { TareasComponent } from './tareas/tareas.component';
import { HomeComponent } from './home/home.component';
import { FormsModule } from '@angular/forms';
import { ServiceTareaService } from './services/serviceTarea.service';
import { ServiceDataService } from './services/service-data.service';
import { ServiceLoginService } from './services/service-login.service';
import { CookieService } from 'ngx-cookie-service';
import { HttpClientModule } from '@angular/common/http';
import { GestorComponent } from './gestor/gestor.component';
import { LoginComponent } from './login/login.component';
import { LoginGuardian } from './login/login-guardian';

@NgModule({
  declarations: [
    AppComponent,
    EtiquetasComponent,
    TareasComponent,
    HomeComponent,
    GestorComponent,
    LoginComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
    ServiceTareaService,
    ServiceDataService,
    ServiceLoginService,
    CookieService,
    LoginGuardian
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
