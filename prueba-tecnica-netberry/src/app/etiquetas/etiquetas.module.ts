import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EtiquetasRoutingModule } from './etiquetas-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    EtiquetasRoutingModule
  ]
})
export class EtiquetasModule {}
