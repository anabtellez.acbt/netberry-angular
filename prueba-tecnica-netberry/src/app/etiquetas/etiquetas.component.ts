import { Component, Input } from '@angular/core';
import { Tarea } from '../tarea.model';

@Component({
  selector: 'app-etiquetas',
  templateUrl: './etiquetas.component.html',
  styleUrls: ['./etiquetas.component.css']
})
export class EtiquetasComponent {
  @Input() tarea:Tarea;
  @Input() i:number;
}
