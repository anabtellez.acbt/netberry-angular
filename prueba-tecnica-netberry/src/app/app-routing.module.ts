import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginGuardian } from './login/login-guardian';

const routes: Routes = [
  { path: '', loadChildren: () => import('./home/home.module').then(m => m.HomeModule) },
  { path: 'tareas', loadChildren: () => import('./tareas/tareas.module').then(m => m.TareasModule), canActivate:[LoginGuardian] },
  { path: 'etiquetas', loadChildren: () => import('./etiquetas/etiquetas.module').then(m => m.EtiquetasModule) },
  { path: 'login', loadChildren: () => import('./login/login.module').then(m => m.LoginModule) },
  { path: 'gestionar/:id', loadChildren: () => import('./gestor/gestor.module').then(m => m.GestorModule) },
  { path: '**', loadChildren: () => import('./error/error.module').then(m => m.ErrorModule) },];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
