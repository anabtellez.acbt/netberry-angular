import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TareasRoutingModule } from './tareas-routing.module';
import { RouterModule, Routes } from '@angular/router';
import { EtiquetasComponent } from '../etiquetas/etiquetas.component';
import { FormsModule } from '@angular/forms';

const routes: Routes = [
  {
    path: '',
    children: [
      {path: 'etiquetas', component: EtiquetasComponent}
    ]
  }
];


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    TareasRoutingModule,
    RouterModule.forChild(routes),
    FormsModule
  ]
})
export class TareasModule { }
