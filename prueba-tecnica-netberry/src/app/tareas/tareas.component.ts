import { Component, OnInit } from '@angular/core';
import { Tarea } from '../tarea.model';
import { ServiceTareaService } from '../services/serviceTarea.service';


@Component({
  selector: 'app-tareas',
  templateUrl: './tareas.component.html',
  styleUrls: ['./tareas.component.css']
})
export class TareasComponent {
  titulo:string = 'Listado de tareas';
  tareaForm = '';
  descForm = '';
  tagForm = '';
  tareas:Tarea[]=[];

  constructor(private servicio:ServiceTareaService) {}

  ngOnInit() {
    this.servicio.obtenerTareas().subscribe(tareas=>{
      this.tareas=Object.values(tareas);
      this.servicio.setTareas(this.tareas);
    });
  }

  annadirTarea() {
    let tareas = new Tarea(this.tareaForm, this.descForm, this.tagForm);
    this.servicio.annadirTarea(tareas);
  }


}
