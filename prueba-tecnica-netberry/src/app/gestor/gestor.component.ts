import { Component, OnInit } from '@angular/core';
import { Tarea } from '../tarea.model';
import { ActivatedRoute, Router } from '@angular/router';
import { ServiceTareaService } from '../services/serviceTarea.service';
import { ServiceDataService } from '../services/service-data.service';

@Component({
  selector: 'app-gestor',
  templateUrl: './gestor.component.html',
  styleUrls: ['./gestor.component.css']
})
export class GestorComponent implements OnInit {
  titulo = 'Gestión';
  tareaForm:string = '';
  descForm:string = '';
  tagForm:string = '';
  id:number;
  tareas:Tarea[]=[];
  accion:number;

  constructor(private router:Router, private servicio:ServiceTareaService, private route:ActivatedRoute, private dataService:ServiceDataService) {}

  ngOnInit(): void {
    this.tareas = this.servicio.tareas;
    this.id=this.route.snapshot.params['id'];
    let tarea = this.tareas[this.id];
    this.tareaForm=tarea.titulo;
    this.descForm=tarea.descripcion;
    this.tagForm=tarea.etiquetas;

    this.accion=Number(this.route.snapshot.queryParams['accion']);
  }

  volverHome() {
    this.router.navigate(['/tareas']);
  }

  actualizarTarea() {
    if (this.accion == 1) {
      let tarea = new Tarea(this.tareaForm, this.descForm, this.tagForm);
      this.servicio.actualizarTarea(this.id, tarea);
      this.router.navigate(['/tareas']);
    } else if (this.accion == 2) {
      this.servicio.eliminarTarea(this.id);
      this.router.navigate(['/tareas']);
    }
  }
}
