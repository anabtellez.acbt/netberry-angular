import { TestBed } from '@angular/core/testing';

import { ServiceTareaService } from './serviceTarea.service';

describe('ServiceTareaService', () => {
  let service: ServiceTareaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ServiceTareaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
