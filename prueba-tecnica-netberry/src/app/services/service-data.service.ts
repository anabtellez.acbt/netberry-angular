import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ServiceLoginService } from './service-login.service';
import { Tarea } from '../tarea.model';

@Injectable({
  providedIn: 'root'
})
export class ServiceDataService {

  constructor(private httpClient:HttpClient, private loginService:ServiceLoginService) { }

  cargarTareas() {
    const token = this.loginService.getIdToken();
    return this.httpClient.get('https://tareas-a7729-default-rtdb.europe-west1.firebasedatabase.app/datos.json?auth=' + token);
  }

  guardarTareas(tareas:Tarea[]) {
    const token = this.loginService.getIdToken();
    this.httpClient.put('https://tareas-a7729-default-rtdb.europe-west1.firebasedatabase.app/datos.json?auth=' + token, tareas).subscribe(
      response=>console.log("Se ha guardado la tarea " + response),
      error=>console.log("Error: " + error)
    );
  }

  actualizarTarea(id:number, tarea:Tarea) {
    const token = this.loginService.getIdToken();
    let url = 'https://tareas-a7729-default-rtdb.europe-west1.firebasedatabase.app/datos/' + id + '.json?auth=' + token;
    this.httpClient.put(url, tarea).subscribe(
      response=>console.log("Se ha modificado correctamente la tarea " + response),
      error=>console.log("Error: " + error)
    );
  }

  eliminarTarea(id:number) {
    const token = this.loginService.getIdToken();
    let url = 'https://tareas-a7729-default-rtdb.europe-west1.firebasedatabase.app/datos/' + id + '.json?auth=' + token;
    this.httpClient.delete(url).subscribe(
      response=>console.log("Se ha eliminado correctamente la tarea " + response),
      error=>console.log("Error: " + error)
    );

  }


}
