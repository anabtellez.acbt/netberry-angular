import { Injectable } from '@angular/core';
import { Tarea } from '../tarea.model';
import { ServiceDataService } from './service-data.service';


@Injectable({
  providedIn: 'root'
})
export class ServiceTareaService {
  tareas:Tarea[] = [];

  constructor(private dataService:ServiceDataService) {}

  setTareas(misTareas:Tarea[]) {
    this.tareas = misTareas;
  }

  obtenerTareas() {
    return this.dataService.cargarTareas();
  }

  annadirTarea(tarea:Tarea) {
    this.tareas.push(tarea);
    this.dataService.guardarTareas(this.tareas);
  }

  seleccionarTarea(id:number) {
    let tarea:Tarea=this.tareas[id];
    console.log(this.tareas);
    return tarea;
  }

  actualizarTarea(id:number, tarea:Tarea) {
    this.tareas[id]=tarea;
    this.dataService.actualizarTarea(id, tarea);
  }

  eliminarTarea(id:number) {
    this.tareas.splice(id,1);
    this.dataService.eliminarTarea(id);
    this.dataService.guardarTareas(this.tareas);
  }


}
