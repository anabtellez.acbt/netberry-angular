import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from "@angular/router";
import { ServiceLoginService } from "../services/service-login.service";
import Swal from "sweetalert2";

@Injectable()
export class LoginGuardian implements CanActivate {

  constructor(private loginService: ServiceLoginService, private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.loginService.checkLogin()) {
      return true;
    } else {
      Swal.fire({
        icon: 'warning',
        title: 'Recurso protegido',
        text: 'Es necesario iniciar sesión para acceder. Está siendo redirigido a la página de login',
        confirmButtonColor: '#0d6efd'
      });
      this.router.navigate(['/login']);
      return false;
    }
  }
}
