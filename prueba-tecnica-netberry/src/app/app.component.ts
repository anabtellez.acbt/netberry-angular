import { Component, OnInit } from '@angular/core';
import firebase from 'firebase/compat/app'
import { ServiceLoginService } from './services/service-login.service';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'prueba-tecnica-netberry';

  constructor(private loginService:ServiceLoginService){}

  ngOnInit(): void {
    firebase.initializeApp({
      apiKey: "AIzaSyDR1hseJPhadNGDmBLwO-7d_MyR07JBXec",
      authDomain: "tareas-a7729.firebaseapp.com",
    });
  }

  checkLogin() {
    return this.loginService.checkLogin();
  }

  logout() {
    this.loginService.logout();
  }
}
